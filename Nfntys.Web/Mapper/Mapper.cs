﻿using Nfntys.Helpers.DTOs;
using Nfntys.Web.Models;
using System.Collections.Generic;

namespace Nfntys.Web.Mapper
{
    public static class Mapper
    {
        public static List<UserViewModel> UserDTOListToUserViewModelList(List<UserDTO> users)
        {
            var result = new List<UserViewModel>();

            foreach (var user in users)
            {
                result.Add(new UserViewModel
                {
                    UserName = user.UserName,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Email = user.Email,
                    Skills = user.Skills,
                });
            }

            return result;
        }
    }
}
