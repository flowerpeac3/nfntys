﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Nfntys.Services.Contracts;
using Nfntys.Web.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Nfntys.Web.Controllers
{
    public class HomeController : Controller
    {
        
        private readonly IUserService _userService;
        public HomeController(IUserService userService)
        {
            this._userService = userService;
        }
        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> WhoWeAre()
        {
            try
            {
                var result = await this._userService.GetAllUsersAsync();

                var whoWeAreViewModel = new WhoWeAreViewModel(Mapper.Mapper.UserDTOListToUserViewModelList(result));

                return View(whoWeAreViewModel);
            }
            catch (Exception)
            {
                return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
            }
           
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
