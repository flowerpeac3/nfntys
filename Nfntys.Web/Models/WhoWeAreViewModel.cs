﻿using Nfntys.Helpers.DTOs;
using System.Collections.Generic;

namespace Nfntys.Web.Models
{
    public class WhoWeAreViewModel
    {
        public WhoWeAreViewModel()
        {

        }
        public WhoWeAreViewModel(List<UserViewModel> users)
        {
            this.Users = users;
        }

        public List<UserViewModel> Users { get; set; } = new List<UserViewModel>();
    }
}
