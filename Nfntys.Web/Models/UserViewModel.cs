﻿using Microsoft.AspNetCore.Identity;

namespace Nfntys.Web.Models
{
    public class UserViewModel : IdentityUser<int>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Skills { get; set; }
    }
}
