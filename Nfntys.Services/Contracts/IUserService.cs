﻿using Nfntys.Helpers.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Nfntys.Services.Contracts
{
    public interface IUserService
    {
        Task<List<UserDTO>> GetAllUsersAsync();
    }
}
