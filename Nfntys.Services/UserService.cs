﻿using Microsoft.EntityFrameworkCore;
using Nfntys.Database.Contracts;
using Nfntys.Helpers.DTOs;
using Nfntys.Helpers.Mapping;
using Nfntys.Helpers.Messages;
using Nfntys.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Nfntys.Services
{
    public class UserService : IUserService
    {
        private readonly INfntysDBContext _context;

        public UserService(INfntysDBContext context)
        {
            this._context = context;
        }

        public async Task<List<UserDTO>> GetAllUsersAsync()
        {
            var result = await this._context.Users
                .ToListAsync();

            if (result.Count == 0)
                throw new Exception(ProjectMessages.userListIsEmpty);

            return Mapper.UserListToUserDTO(result);
        }
        
        public async Task<UserDTO> GetUserAsync(string username)
        {
            var user = await this._context.Users
                .FirstOrDefaultAsync(u => u.UserName.ToLower().Equals(username.ToLower()));

            if (user == null)
                throw new Exception(ProjectMessages.userNotFound);

            return new UserDTO(user);
        }
    }
}
