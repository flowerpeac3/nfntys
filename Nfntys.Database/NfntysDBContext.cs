﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Nfntys.Database.Contracts;
using Nfntys.Database.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Nfntys.Database
{
    public class NfntysDBContext : IdentityDbContext<User, Role, int>, INfntysDBContext
    {
        public NfntysDBContext()
        {

        }
        public NfntysDBContext(DbContextOptions<NfntysDBContext> options)
            : base(options)
        {

        }
        public override DbSet<User> Users { get; set; }
        public override DbSet<Role> Roles { get; set; }
		protected override void OnModelCreating(ModelBuilder builder)
        {
			this.Seed(builder);

			base.OnModelCreating(builder);
        }
		public async Task<int> SaveChangesAsync()
		{
			return await base.SaveChangesAsync(); 
		}
		protected void Seed(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Role>().HasData(
                new Role() { Id = 1, Name = "Admin", NormalizedName = "ADMIN" },
                new Role() { Id = 2, Name = "User", NormalizedName = "USER" }
                );

			// Password hasher
			var passHasher = new PasswordHasher<User>();

			// Admin
			var adminUser = new User();
			adminUser.Id = 1;
			adminUser.UserName = "_dxeqtr;";
			adminUser.NormalizedUserName = "_DXEQTR;";
			adminUser.Email = "admin@admin.com";
			adminUser.NormalizedEmail = "ADMIN@ADMIN.COM";
			adminUser.PasswordHash = passHasher.HashPassword(adminUser, "123456");
			adminUser.SecurityStamp = Guid.NewGuid().ToString();
			modelBuilder.Entity<User>().HasData(adminUser);

			// Link Role & User (for Admin)
			var adminUserRole = new IdentityUserRole<int>();
			adminUserRole.RoleId = 1;
			adminUserRole.UserId = adminUser.Id;
			modelBuilder.Entity<IdentityUserRole<int>>().HasData(adminUserRole);

			// Regular User
			var regularUser = new User();
			regularUser.Id = 2;
			regularUser.FirstName = "Toncho";
			regularUser.UserName = "user@user.com";
			regularUser.NormalizedUserName = "USER@USER.COM";
			regularUser.Email = "user@user.com";
			regularUser.NormalizedEmail = "USER@USER.COM";
			regularUser.PasswordHash = passHasher.HashPassword(regularUser, "user123");
			regularUser.SecurityStamp = Guid.NewGuid().ToString();
			modelBuilder.Entity<User>().HasData(regularUser);

			// Link Role & User (for Regular User)
			var regularUserRole = new IdentityUserRole<int>();
			regularUserRole.RoleId = 2;
			regularUserRole.UserId = regularUser.Id;
			modelBuilder.Entity<IdentityUserRole<int>>().HasData(regularUserRole);
		}
    }
}
