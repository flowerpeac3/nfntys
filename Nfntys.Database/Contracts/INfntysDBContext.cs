﻿using Microsoft.EntityFrameworkCore;
using Nfntys.Database.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Nfntys.Database.Contracts
{
    public interface INfntysDBContext
    {
        DbSet<User> Users { get; set; }
        DbSet<Role> Roles { get; set; }

        Task<int> SaveChangesAsync();
    }
}
