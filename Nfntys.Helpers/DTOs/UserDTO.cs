﻿using Nfntys.Database.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nfntys.Helpers.DTOs
{
    public class UserDTO
    {
        public UserDTO(User user)
        {
            this.UserName = user.UserName;
            this.Email = user.Email;
            this.FirstName = user.FirstName;
            this.LastName = user.LastName;
            this.Skills = user.Skills;
        }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Skills { get; set; }
    }
}
