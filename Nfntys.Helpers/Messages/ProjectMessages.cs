﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nfntys.Helpers.Messages
{
    public class ProjectMessages
    {
        public const string userListIsEmpty = "User list is empty.";
        public const string userNotFound = "User not found.";
    }
}
