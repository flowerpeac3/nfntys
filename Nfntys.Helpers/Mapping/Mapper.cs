﻿using Nfntys.Database.Models;
using Nfntys.Helpers.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nfntys.Helpers.Mapping
{
    public static class Mapper
    {
        public static List<UserDTO> UserListToUserDTO(List<User> users)
        {
            var result = new List<UserDTO>();

            foreach (var user in users)
            {
                result.Add(new UserDTO(user));
            }

            return result;
        }
    }
}
